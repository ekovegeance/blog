---
title: Definisi Jaringan Komputer menurut Eko Saputra ( Saya sendiri )
date: 2021-02-16T22:37:31Z
hero_image: "../images/undraw_connected_world_wuay.png"
author: Eko Saputra

---
Jaringan komputer adalah terhubungnya dua komputer atau lebih, sehingga menciptakan koneksi untuk berbagi informasi sumberdaya, dan melakukan pertukaran data.